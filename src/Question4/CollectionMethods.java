package Question4;
import java.util.ArrayList;
import java.util.Collection;
import Question3.Planet;

/**
 * This method verifies the list of planets and returns the ones
 * that are bigger than the specific size.
 * @author Rodrigo Rivas 1910674*/

public class CollectionMethods {

	public static Collection<Planet> getLargerThan(Collection<Planet>planets,double size){
		
		ArrayList<Planet> p = new ArrayList<Planet>();
		
		for(Planet pl:planets) {
			if(pl.getRadius()>=size) {
				p.add(pl);
			}
		}
		
		return p;
		
	}
}
