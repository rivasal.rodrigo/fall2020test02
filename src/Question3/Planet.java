package Question3;

import java.util.Objects;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	
	/**Overriding the equals method
	 * so it can verify if the name and the planet system
	 * are the same
	 * @author Rodrigo Rivas 1910674
	 * */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Planet)) {
			return false;
		}
		Planet p = (Planet)o;
		return this.name == p.name && this.planetarySystemName == p.planetarySystemName;
	}
	
	/*
	 * 
	 * */
	
	/**Overriding the hashCode method
	 * so it be based on its name and planetary system
	 * @author Rodrigo Rivas 1910674
	 * */
	@Override
	public int hashCode() {
		return Objects.hash(name,planetarySystemName);
	}
	
	
	/**This methods override the compareTo method so it can compare
	 * based on the planet's name and radius
	 * @author Rodrigo Rivas 1910674
	 * */
	@Override
	public int compareTo(Object o){
		Planet p = (Planet)o;
		int nameComp = this.getName().compareTo(p.getName());
		if(nameComp == 0) {
			//10		5
			if(this.getRadius()>p.getRadius()) {return -1;}
			if(this.getRadius()<p.getRadius()) {return 1;}
			return 0;
		}
		return nameComp;
	}
	
}
