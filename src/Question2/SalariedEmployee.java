package Question2;
/*SalaiedEmployee class implements the employee interface
 * it returns the weekly salary based in the year income.
 * @author Rodrigo Rivas 1910674
 */
public class SalariedEmployee implements Employee{
	private double yearlySalary;
	
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	@Override
	public double getWeeklypay() {
		double weeksYear = 52;
		return yearlySalary/weeksYear;
	}
	
	public double getYearlySalary() {
		return yearlySalary;
	}

}
