package Question2;
/**
 * This class creates different types of employees
 * and calculates the weekly expenses for the company
 * @author Rodrigo Rivas 1910674
 */
public class PayrollManagement {

	public static double getTotalExpenses(Employee[] emp) {
		double total=0;
		
		for(Employee e : emp) {
			total += e.getWeeklypay();
		}
		
		return total;
	}
	
	public static void main (String[] args) {
		Employee[] emp = new Employee[5];
		emp[0] = new SalariedEmployee(15500);
		emp[1] = new HourlyEmployee(23,14);
		emp[2] = new UnionizedHourlyEmployee(45,10,30,1.5);
		emp[3] = new HourlyEmployee(25,16);
		emp[4] = new UnionizedHourlyEmployee(45,14,30,2);
		
		System.out.println("Total weekly Expenses: " + getTotalExpenses(emp));
	}
	
}
