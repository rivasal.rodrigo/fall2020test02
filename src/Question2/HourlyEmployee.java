package Question2;
/*
 * @author Rodrigo Rivas 1910674
 */
public class HourlyEmployee implements Employee{
	private double workedHoursWeek;
	private double hourRate;

	public HourlyEmployee(double workedHoursWeek, double hourRate) {
		this.workedHoursWeek = workedHoursWeek;
		this.hourRate = hourRate;
	}
	
	@Override
	public double getWeeklypay() {
		return workedHoursWeek * hourRate;
	}
	
	public double getWorkedHoursWeek() {
		return workedHoursWeek;
	}
	
	public double getHourRate() {
		return hourRate;
	}

}
