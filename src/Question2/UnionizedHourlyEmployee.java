package Question2;
/**
 * This class calculates the overtime and adds it to the regular
 * weekly hours paid at the regular rate
 * @author Rodrigo Rivas 1910674
 */
public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double workedHoursWeek, double hourRate, double maxHoursPerWeek, double overtimeRate) {
		super(workedHoursWeek, hourRate);//45,10$
		this.maxHoursPerWeek = maxHoursPerWeek;//30
		this.overtimeRate = overtimeRate;//1.5
	}
	
	@Override
	public double getWeeklypay() {
		//45<=30
		if(super.getWorkedHoursWeek()<= maxHoursPerWeek) {
			return super.getWeeklypay();
		}						
		return maxHoursPerWeek *super.getHourRate() + (super.getWorkedHoursWeek()-maxHoursPerWeek)*super.getHourRate()*overtimeRate;
	}

}
